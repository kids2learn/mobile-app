## Inspiration

We all can sympathize with the difficulties concerning the digital learning during this Covid Pandemia. Lot of us have kids (especially little pupils) and struggle everyday to accomplish all their educational tasks. It's absolutely not easy to attend all the lessons, remember the rolling schedule of the digital meeting, homework  to send, corrupted invitation links, difficulties in take speech  during the lessons, low attention... everything happen while the parents are  cooking, cleaning the house, traveling around groceries, work (phisical or smartworking), fighting the boredom for yourself and your dear ones. All those problems are even bigger if your child is 6-7yo, as digital e-learning platforms were born in an Age without the Covid-19 and built as a support for "older" children (14yo and above). 
So, one day, we thought *"Hey, what if we create an App to support all those challanges?"*. And so we did! 
This is our aim: **supporting parents and pupils in using e-learning platforms in a more feasible and intuitive way**. 
And that was the start of **KidsToLearn**. A new star is born.

## What it does
It's aimed to help students, teachers and families through specific tools and functionalities:

 - *RISE YOUR HAND BUTTON*: Students can push a big red button to start asking and speaking, and so they will be able to recall the teacher's attention directly on her smartphone during the e-learning session.
- *LESSONS' CALENDAR*: Teachers can create an online calendar for their classes and send an alert notification to their students as a memorandum or if a student isn't online during the scheduled lesson.   
- *EMOTICONS& STICKERS*: Teachers can send positive or neagative emoticons&stickers to their students in real-time to positive or negative underline their behavior during the lesson and claim their attention.
- *URL SHORTENER*: To help students and parents to access the lesson, the teacher can send a direct, simple and extremely short link, with a minimal length of simple and mnemonic words, to make the lesson's access easier.
- *GAMING*: Self assessment and evaluation Peer-to-peer gaming and rewarded in class-shared Leaderboards. The teacher can decide to launch the game at any time, eg to start the competitions at the beginning of the lesson  to test the profit of their students in the homework phase or, for instance at the closing of the lesson  to test their students' level of attention during the lesson itself. The game is enriched with a competitive leaderboard and virtual trophies that gain the students will in doing better. 

## How we built it

In progress.

## Challenges we ran into

In progress.

## Accomplishments that we're proud of 

 - The KidsToLearn idea perfectly address the OECD ongoing challenges related to the education sector during Covid19 pandemia.
 - Remote working with a EU team.

## What we learned

 - Mixing many professionals ideas, cooperation between different people, different professions an fields.
 - New tools, new partners, new ideas!

## What's next for KidsToLearn

Students and parents will be able to access all over the world, even when the CovidVirus will be defeated. Indeed, it will be an useful support for the daily learning, especially when a student isn't able to attend phisically the lesson. 
