import 'package:flutter/material.dart';
import 'package:kidslearn/screens/home_screen.dart';
import 'package:kidslearn/screens/login_user_screen.dart';
import 'package:kidslearn/screens/register_user_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Classroom UI',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: Colors.white,
          backgroundColor: Colors.lightBlue,
          accentColor: Colors.orangeAccent),
      darkTheme: ThemeData.dark(),
      initialRoute: HomeScreen.id,
      routes: {
        HomeScreen.id: (context) => HomeScreen(),
        RegisterUserScreen.id: (context) => RegisterUserScreen(),
        LoginUserScreen.id: (context) => LoginUserScreen(),
      },
    );
  }
}
