import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kidslearn/models/class_model.dart';
import 'package:kidslearn/screens/prof_class_screen.dart';

class FirebaseClassList extends StatelessWidget {
  final Stream<QuerySnapshot> querySnap;
  final Color color;

  FirebaseClassList({this.querySnap, this.color});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          child: StreamBuilder<QuerySnapshot>(
              stream: querySnap,
              builder: (BuildContext context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                    child: CircularProgressIndicator(),
                  );

                List<DocumentSnapshot> classes = snapshot.data.documents;
                return ListView.builder(
                  itemCount: classes.length,
                  itemBuilder: (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) => ProfClassScreen(
                                  cls: Class.fromJson(classes[index].data)))),
                      child: Container(
                          decoration: BoxDecoration(
                              color: color,
                              borderRadius: BorderRadius.circular(30.0)),
                          padding: EdgeInsets.symmetric(
                              horizontal: 20.0, vertical: 15.0),
                          margin: EdgeInsets.only(
                            top: 10.0,
                            bottom: 5.0,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(classes[index]['name']),
                              IconButton(
                                icon: Icon(Icons.settings),
                                onPressed: () {},
                              ),
                            ],
                          )),
                    );
                  },
                );
              }),
        ),
      ],
    );
  }
}
