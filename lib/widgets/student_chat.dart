import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class StudentChat extends StatefulWidget {
  final Future<QuerySnapshot> chatQuery;

  StudentChat({this.chatQuery});

  @override
  _StudentChatState createState() => _StudentChatState();
}

class _StudentChatState extends State<StudentChat> {
  final Firestore _firestore = Firestore.instance;

  ScrollController scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Expanded(
        child: FutureBuilder<QuerySnapshot>(
            future: widget.chatQuery,
            builder: (BuildContext context, snapshot) {
              if (!snapshot.hasData)
                return Center(
                  child: CircularProgressIndicator(),
                );

              if (snapshot.data.documents.length > 0)
                return StreamBuilder<QuerySnapshot>(
                  stream: _firestore
                      .collection('chats')
                      .document(snapshot.data.documents[0].documentID)
                      .collection('messages')
                      .orderBy('ts')
                      .snapshots(),
                  builder: (BuildContext context, snapshot) {
                    if (!snapshot.hasData)
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    List<DocumentSnapshot> docs = snapshot.data.documents;
                    List<Widget> messages = docs
                        .map(
                          (doc) => Row(
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    constraints: BoxConstraints(
                                        maxWidth:
                                            MediaQuery.of(context).size.width *
                                                .6),
                                    padding: const EdgeInsets.all(15.0),
                                    decoration: BoxDecoration(
                                      color: Color(0xfff9f9f9),
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(25),
                                        bottomLeft: Radius.circular(25),
                                        bottomRight: Radius.circular(25),
                                      ),
                                    ),
                                    child: Text(
                                      doc['text'],
                                      style: Theme.of(context)
                                          .textTheme
                                          .body1
                                          .apply(
                                            color: Colors.black87,
                                          ),
                                    ),
                                  ),
                                  SizedBox(height: 15.0),
                                ],
                              ),
                              SizedBox(width: 15),
                              Text(
                                "${DateTime.fromMillisecondsSinceEpoch(doc['ts']).toLocal()}",
                                style: Theme.of(context)
                                    .textTheme
                                    .body2
                                    .apply(color: Colors.black87),
                              ),
                            ],
                          ),
                        )
                        .toList();
                    return Container(
                      margin:
                          EdgeInsets.only(left: 20.0, right: 20.0, top: 15.0),
                      child: Container(
                        margin:
                            EdgeInsets.only(left: 20.0, right: 20.0, top: 15.0),
                        child: ListView(
                          controller: scrollController,
                          children: <Widget>[
                            ...messages,
                          ],
                        ),
                      ),
                    );
                  },
                );
              else
                return ListView(
                  controller: scrollController,
                  children: <Widget>[],
                );
            }),
      ),
    );
  }
}
