import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';

class ProfCalendar extends StatefulWidget {
  final Color color;
  
  ProfCalendar({this.color});

  @override
  _ProfCalendarState createState() => _ProfCalendarState();
}

class _ProfCalendarState extends State<ProfCalendar> {
  CalendarController _calendarController;

  @override
  void initState() {
    super.initState();
    _calendarController = CalendarController();
  }

  @override
  void dispose() {
    _calendarController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TableCalendar(
        calendarController: _calendarController,
        rowHeight: 50.0,
      ),
      decoration: BoxDecoration(
          color: widget.color,
          borderRadius: BorderRadius.circular(30.0)),
    );
  }
}
