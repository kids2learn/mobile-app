import 'package:flutter/material.dart';
import 'package:kidslearn/models/user_model.dart';
import 'package:kidslearn/screens/join_class_screen.dart';
import 'package:kidslearn/screens/prof_manage_classes_screen.dart';
import 'package:kidslearn/screens/student_attend_class_screen.dart';
import 'package:kidslearn/utils.dart';

class NavDrawer extends StatefulWidget {
  @override
  _NavDrawerState createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  @override
  Widget build(BuildContext context) {
    final User user = getCurrentUser();

    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: <Widget>[
        Container(
          color: Colors.orange,
          child: DrawerHeader(
            child: Row(
              children: <Widget>[
                CircleAvatar(
                  radius: 35.0,
                  backgroundColor: Theme.of(context).primaryColor,
                  backgroundImage: AssetImage('assets/images/logo.png'),
                ),
                SizedBox(width: 15.0),
                Text(user.name,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
              ],
            ),
          ),
        ),
        Container(
          color: Colors.orangeAccent,
          child: ListTile(
            leading: Icon(Icons.featured_play_list),
            title: Text(user.isProf ? 'My Classes' : 'My Class'),
            onTap: () => {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => user.isProf
                          ? ProfManageClassesScreen()
                          : (user.classes != null && user.classes.length > 0)
                              ? StudentAttendClassScreen()
                              : JoinClassScreen()))
            },
          ),
        ),
      ],
    ));
  }
}
