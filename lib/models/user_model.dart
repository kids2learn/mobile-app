import 'package:kidslearn/models/class_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';

@JsonSerializable()
class User {
  final String uid;
  final String name;
  final List<String> classes;
  final String email;

  @JsonKey(name: 'is_prof')
  final bool isProf;

  User({this.uid, this.name, this.classes, this.isProf, this.email});

  factory User.fromJson(Map<String, dynamic> json) => _$UserFromJson(json);

  Map<String, dynamic> toJson() => _$UserToJson(this);
}
