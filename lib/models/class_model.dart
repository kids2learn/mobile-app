import 'package:json_annotation/json_annotation.dart';

part 'class_model.g.dart';


@JsonSerializable()

class Class {
  final int id;
  final String name;
  @JsonKey(name: 'studs')
  final List<String> alumniIds;
  @JsonKey(name: 'profs')
  final List<String> profsIds;

  Class({this.id, this.name, this.alumniIds, this.profsIds});

  factory Class.fromJson(Map<String, dynamic> json) => _$ClassFromJson(json);

  Map<String, dynamic> toJson() => _$ClassToJson(this);

}