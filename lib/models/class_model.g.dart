// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'class_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Class _$ClassFromJson(Map<String, dynamic> json) {
  return Class(
    id: json['id'] as int,
    name: json['name'] as String,
    alumniIds: (json['studs'] as List)?.map((e) => e as String)?.toList(),
    profsIds: (json['profs'] as List)?.map((e) => e as String)?.toList(),
  );
}

Map<String, dynamic> _$ClassToJson(Class instance) => <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'studs': instance.alumniIds,
      'profs': instance.profsIds,
    };
