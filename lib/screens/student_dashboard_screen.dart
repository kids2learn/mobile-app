import 'package:flutter/material.dart';
import 'package:kidslearn/screens/new_class_screen.dart';
import 'package:kidslearn/widgets/nav_drawer.dart';
import 'package:kidslearn/widgets/prof_calendar.dart';

class StudentDashboardScreen extends StatefulWidget {

  @override
  _StudentDashboardScreenState createState() => _StudentDashboardScreenState();
}

List<String> opts = ['Create New Class', 'opt2', 'opt2'];
Map<String, int> optsMapping = {'Create New Class': 0};

class _StudentDashboardScreenState extends State<StudentDashboardScreen> {
  _onSelectedOptChoice(int opt) {
    switch (opt) {
      case 0:
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => NewClassScreen(
                    )));
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    final ProfCalendar calendar = ProfCalendar(color: Theme.of(context).primaryColor,);
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text(
          'Student Dashboard',
          style: TextStyle(
            fontSize: 28.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 10.0),
        child: Column(
          children: <Widget>[
            calendar,
          ],
        ),
      ),
    );
  }
}
