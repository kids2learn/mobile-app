import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kidslearn/models/user_model.dart';
import 'package:kidslearn/screens/prof_dashboard_screen.dart';
import 'package:kidslearn/screens/student_dashboard_screen.dart';
import 'package:kidslearn/utils.dart';
import 'package:kidslearn/widgets/custom_button.dart';

class LoginUserScreen extends StatefulWidget {
  static const String id = 'LOGINUSER';

  @override
  _LoginUserScreenState createState() => _LoginUserScreenState();
}

class _LoginUserScreenState extends State<LoginUserScreen> {
  String email;
  String password;
  String role;

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _firestore = Firestore.instance;

  _signInUser() async {
    AuthResult res = await _auth.signInWithEmailAndPassword(
        email: email, password: password);

    if (res.user != null) {
      await _firestore
          .collection('users')
          .document(res.user.uid)
          .get()
          .then((query) {
        User user = User.fromJson(query.data);
        setCurrentUser(user);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => user.isProf
                    ? ProfDashboardScreen()
                    : StudentDashboardScreen()));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('KidsToLearn'),
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                keyboardType: TextInputType.emailAddress,
                onChanged: (value) => email = value,
                decoration: InputDecoration(
                    hintText: 'Enter Your Email', border: OutlineInputBorder()),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                obscureText: true,
                autocorrect: false,
                onChanged: (value) => password = value,
                decoration: InputDecoration(
                    hintText: 'Enter Your Password',
                    border: OutlineInputBorder()),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              margin: EdgeInsets.only(bottom: 40.0),
              child: CustomButton(
                color: Theme.of(context).accentColor,
                text: 'Log In',
                callback: _signInUser,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
