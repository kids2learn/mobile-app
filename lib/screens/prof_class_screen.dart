import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kidslearn/models/class_model.dart';
import 'package:kidslearn/screens/prof_chat_screen.dart';
import 'package:kidslearn/utils.dart';
import 'package:kidslearn/widgets/nav_drawer.dart';

class ProfClassScreen extends StatefulWidget {
  final Class cls;

  ProfClassScreen({this.cls});
  @override
  _ProfClassWidgetState createState() => _ProfClassWidgetState();
}

class _ProfClassWidgetState extends State<ProfClassScreen> {
  final Firestore _firestore = Firestore.instance;
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          iconSize: 30.0,
          onPressed: () {},
        ),
        title: Text(
          widget.cls.name,
          style: TextStyle(
            fontSize: 28.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.notifications_none),
            iconSize: 30.0,
            onPressed: () {},
          ),
        ],
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                child: ClipRRect(
                  child: Container(
                    child: ListView.builder(
                      itemBuilder: (BuildContext context, int index) {
                        return FutureBuilder<DocumentSnapshot>(
                          future: _firestore.collection('users').document(widget.cls.alumniIds[index]).get(),
                            builder: (context, snapshot) {
                          if (!snapshot.hasData)
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          return Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 20.0, vertical: 10.0),
                              margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
                              decoration: BoxDecoration(
                                  color:
                                      Theme.of(context).primaryColor,
                                  borderRadius: BorderRadius.circular(30.0)),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  GestureDetector(
                                    onTap: () => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (_) => ProfChatScreen(
                                                studentId: widget
                                                    .cls.alumniIds[index]))),
                                    child: Container(
                                      child: Row(
                                        children: <Widget>[
                                          CircleAvatar(
                                            radius: 35.0,
                                            backgroundColor:
                                                Theme.of(context).primaryColor,
                                            backgroundImage: AssetImage(
                                                'assets/images/logo.png'),
                                          ),
                                          SizedBox(
                                            width: 10.0,
                                          ),
                                          Text(
                                              snapshot.data.data['name']),
                                        ],
                                      ),
                                    ),
                                  ),
                                  RaisedButton(
                                    color: Theme.of(context).accentColor,
                                    onPressed: () {},
                                    child: Text('notify'),
                                  ),
                                ],
                              ));
                        });
                      },
                      itemCount: widget.cls.alumniIds.length,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              child: RaisedButton(
                color: Theme.of(context).accentColor,
                onPressed: () {},
                child: Text('Notify all students'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
