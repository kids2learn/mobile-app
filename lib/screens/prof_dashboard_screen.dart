import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kidslearn/widgets/firebase_class_list.dart';
import 'package:kidslearn/models/user_model.dart';
import 'package:kidslearn/screens/new_class_screen.dart';
import 'package:kidslearn/utils.dart';
import 'package:kidslearn/widgets/nav_drawer.dart';
import 'package:kidslearn/widgets/prof_calendar.dart';

class ProfDashboardScreen extends StatefulWidget {
  @override
  _ProfDashboardScreenState createState() => _ProfDashboardScreenState();
}

List<String> opts = ['Create New Class', 'opt2', 'opt2'];
Map<String, int> optsMapping = {'Create New Class': 0};

class _ProfDashboardScreenState extends State<ProfDashboardScreen> {
  final Firestore _firestore = Firestore.instance;

  _onSelectedOptChoice(int opt) {
    switch (opt) {
      case 0:
        Navigator.push(
            context, MaterialPageRoute(builder: (_) => NewClassScreen()));
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    final ProfCalendar calendar = ProfCalendar(color: Theme.of(context).primaryColor);
    final User prof = getCurrentUser();
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text(
          'Prof Dashboard',
          style: TextStyle(
            fontSize: 28.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 10.0),
        child: Column(
          children: <Widget>[
            calendar,
            Expanded(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                child: FirebaseClassList(
                    color: Theme.of(context).primaryColor,
                    querySnap: _firestore
                        .collection('classes')
                        .where('profs', arrayContains: prof.uid)
                        .snapshots()),
              ),
            )
          ],
        ),
      ),
    );
  }
}
