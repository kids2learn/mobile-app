import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kidslearn/models/user_model.dart';
import 'package:kidslearn/utils.dart';
import 'package:kidslearn/widgets/nav_drawer.dart';
import 'package:kidslearn/widgets/student_chat.dart';

class ProfChatScreen extends StatefulWidget {
  final String studentId;

  ProfChatScreen({this.studentId});

  @override
  _ProfChatScreenState createState() => _ProfChatScreenState();
}

class _ProfChatScreenState extends State<ProfChatScreen> {
  final Firestore _firestore = Firestore.instance;

  final User prof = getCurrentUser();

  ScrollController scrollController = ScrollController();
  TextEditingController messageController = TextEditingController();

  Future<void> _textCallback() async {
    if (messageController.text.length > 0) {
      DocumentSnapshot doc;
      String docIdIfNull;
      await _firestore
          .collection('chats')
          .where('prof', isEqualTo: prof.uid)
          .where('student', isEqualTo: widget.studentId)
          .getDocuments()
          .then((query) {
        if (query.documents != null && query.documents.length > 0)
          doc = query.documents[0];
      });

      if(doc == null) {
          await _firestore.collection('chats').add({
            'prof': prof.uid,
            'student': widget.studentId,
          }).then((query) {
              docIdIfNull = query.documentID;
          });
      }
      await _firestore
          .collection('chats')
          .document(doc != null? doc.documentID:docIdIfNull)
          .collection('messages')
          .add({
        'text': messageController.text,
        'from': prof.uid,
        'to': widget.studentId,
        'ts': DateTime.now().toUtc().millisecondsSinceEpoch,
      });
      messageController.clear();

      setState(() {
      });
      //scrollController.animateTo(scrollController.position.maxScrollExtent,
          //curve: Curves.easeOut, duration: Duration(milliseconds: 300));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          iconSize: 30.0,
          onPressed: () {},
        ),
        title: Text(
          'Chat with Student',
          style: TextStyle(
            fontSize: 28.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.notifications_none),
            iconSize: 30.0,
            onPressed: () {},
          ),
        ],
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        child: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              StudentChat(
                chatQuery: _firestore
                    .collection('chats')
                    .where('prof', isEqualTo: prof.uid)
                    .where('student', isEqualTo: widget.studentId)
                    .getDocuments(),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        color: Theme.of(context).primaryColor,
                        child: TextField(
                          onSubmitted: (_) => _textCallback(),
                          decoration: InputDecoration(
                              hintText: 'Enter A Messaege ...',
                              border: const OutlineInputBorder()),
                          controller: messageController,
                        ),
                      ),
                    ),
                    IconButton(
                      color: Colors.black,
                      onPressed: _textCallback,
                      icon: Icon(Icons.send),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
