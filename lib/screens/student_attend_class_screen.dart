import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kidslearn/models/class_model.dart';
import 'package:kidslearn/screens/join_class_screen.dart';
import 'package:kidslearn/utils.dart';
import 'package:kidslearn/widgets/nav_drawer.dart';
import 'package:kidslearn/widgets/student_chat.dart';

class StudentAttendClassScreen extends StatefulWidget {
  final Class cls;

  StudentAttendClassScreen({this.cls});

  @override
  _StudentAttendClassScreenState createState() =>
      _StudentAttendClassScreenState();
}

List<String> opts = ['op1', 'opt2', 'opt2'];
Map<String, int> optsMapping = {'op1': 0};

class _StudentAttendClassScreenState extends State<StudentAttendClassScreen> {
  final Firestore _firestore = Firestore.instance;

  _onSelectedOptChoice(int opt) {
    switch (opt) {
      case 0:
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (_) => JoinClassScreen(
                    )));
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    final student = getCurrentUser();

    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text(
          'Class'/*widget.cls.name*/,
          style: TextStyle(
            fontSize: 28.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          PopupMenuButton<int>(
            itemBuilder: (BuildContext context) {
              return opts.map((String opt) {
                return PopupMenuItem<int>(
                    value: optsMapping[opt],
                    child: ListTile(
                      title: Text(opt),
                    ));
              }).toList();
            },
            onSelected: _onSelectedOptChoice,
          )
        ],
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            StudentChat(
              chatQuery: _firestore
              .collection('chats')
              .where('student', isEqualTo: student.uid).getDocuments()),
            Container(
              padding: EdgeInsets.symmetric(vertical: 20.0),
              margin: EdgeInsets.only(bottom: 20.0),
                child: ButtonTheme(
                    minWidth: 200,
                    height: 50,
                    child: RaisedButton(
                      color: Theme.of(context).accentColor,
                      onPressed: () {},
                      child: Text('Notify teacher'),
                    ))),
          ],
        ),
      ),
    );
  }
}
