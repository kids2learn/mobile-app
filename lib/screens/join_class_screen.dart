import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kidslearn/models/user_model.dart';
import 'package:kidslearn/screens/new_class_screen.dart';
import 'package:kidslearn/utils.dart';
import 'package:kidslearn/widgets/nav_drawer.dart';

class JoinClassScreen extends StatefulWidget {
  @override
  _JoinClassScreenState createState() => _JoinClassScreenState();
}

class _JoinClassScreenState extends State<JoinClassScreen> {
  String ids, password;
  final Firestore _firestore = Firestore.instance;

  TextEditingController idsController = TextEditingController();
  TextEditingController passController = TextEditingController();

  final User student = getCurrentUser();

  _joinClass() async {
    if (idsController.text.length > 0) {
      List<String> idsArr = ids.split(',');
      if (passController.text.length > 0 && idsArr.length == 3) {
        var doc;

        await _firestore
            .collection('classes')
            .where('id1', isEqualTo: idsArr[0])
            .where('id2', isEqualTo: idsArr[1])
            .where('id3', isEqualTo: idsArr[2])
            .getDocuments()
            .then((query) {
              if(query.documents != null && query.documents.length > 0)
                doc = query.documents[0];
        });

        var data = doc.data;
        List<String> studs = (data['studs'] as List<dynamic>).map((item) => item as String).toList();
        studs.add(student.uid);
        data['studs'] = studs;
        await _firestore.collection('classes').document(doc.documentID).updateData(data);

        await _firestore.collection('users').document(student.uid).get().then((query) {
          User upUser = User.fromJson(query.data);
          upUser.classes.add(doc.documentID);
          _firestore.collection('users').document(student.uid).setData(upUser.toJson());
        });

        Navigator.pop(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        title: Text(
          'Join A Class',
          style: TextStyle(
            fontSize: 28.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(horizontal: 60.0),
              margin: EdgeInsets.only(top: 20.0),
              height: 200,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    color: Theme.of(context).primaryColor,
                    child: TextField(
                      onChanged: (value) => ids = value,
                      decoration: InputDecoration(
                          hintText: 'Enter Class ID',
                          border: OutlineInputBorder()),
                      controller: idsController,
                    ),
                  ),
                  SizedBox(height: 10.0),
                  Container(
                    color: Theme.of(context).primaryColor,
                    child: TextField(
                      onChanged: (value) => password = value,
                      decoration: InputDecoration(
                          hintText: 'Enter Class Passphrase',
                          border: OutlineInputBorder()),
                      controller: passController,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 60.0),
              margin: EdgeInsets.only(bottom: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox.shrink(),
                  Container(
                    child: ButtonTheme(
                      minWidth: 200,
                      height: 50,
                      child: RaisedButton(
                        color: Theme.of(context).accentColor,
                        onPressed: _joinClass,
                        child: Container(child: Text('Join Class')),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
