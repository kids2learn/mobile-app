import 'package:flutter/material.dart';
import 'package:kidslearn/screens/login_user_screen.dart';
import 'package:kidslearn/screens/register_user_screen.dart';
import 'package:kidslearn/widgets/custom_button.dart';

class HomeScreen extends StatefulWidget {
  static const String id = 'HOME';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Theme.of(context).primaryColor,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Hero(
                    tag: 'full-logo',
                    child: Container(
                      child: Image.asset('assets/images/kidstolearn.png'),
                    ),
                  ),
                ],
              ),
            ),
            Column(
              children: <Widget>[
                CustomButton(
                  color: Theme.of(context).accentColor,
                  text: 'Log In',
                  callback: () {
                    Navigator.of(context).pushNamed(LoginUserScreen.id);
                  },
                ),
                SizedBox(height: 20.0),
                CustomButton(
                  color: Theme.of(context).backgroundColor,
                  text: 'Register',
                  callback: () {
                    Navigator.of(context).pushNamed(RegisterUserScreen.id);
                  },
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
