import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kidslearn/models/user_model.dart';
import 'package:kidslearn/screens/prof_dashboard_screen.dart';
import 'package:kidslearn/screens/student_dashboard_screen.dart';
import 'package:kidslearn/utils.dart';
import 'package:kidslearn/widgets/custom_button.dart';

class RegisterUserScreen extends StatefulWidget {
  static const String id = 'REGISTERUSER';

  @override
  _RegisterUserScreenState createState() => _RegisterUserScreenState();
}

class _RegisterUserScreenState extends State<RegisterUserScreen> {
  String email;
  String password;
  String role;
  String name;

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _firestore = Firestore.instance;

  _registerUser() async {
    AuthResult res = await _auth.createUserWithEmailAndPassword(
        email: email, password: password);

    if (res.user != null) {
      final User user = User(
          classes: [],
          email: email,
          isProf: role == 'Teacher',
          name: name,
          uid: res.user.uid);
      await _firestore
          .collection('users')
          .document(res.user.uid)
          .setData(user.toJson());
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('KidsToLearn'),
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                onChanged: (value) => name = value,
                decoration: InputDecoration(
                    hintText: 'Enter Your Name', border: OutlineInputBorder()),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                keyboardType: TextInputType.emailAddress,
                onChanged: (value) => email = value,
                decoration: InputDecoration(
                    hintText: 'Enter Your Email', border: OutlineInputBorder()),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                obscureText: true,
                autocorrect: false,
                onChanged: (value) => password = value,
                decoration: InputDecoration(
                    hintText: 'Enter Your Password',
                    border: OutlineInputBorder()),
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    child: DropdownButton<String>(
                        focusColor: Theme.of(context).primaryColor,
                        icon: Icon(Icons.arrow_downward),
                        iconSize: 24,
                        //elevation: 16,
                        value: role,
                        style: TextStyle(
                          color: Colors.black,
                        ),
                        hint: Text(
                          'Select Role',
                          style: TextStyle(
                            color: Colors.black,
                          ),
                        ),
                        items: ['Teacher', 'Student']
                            .map<DropdownMenuItem<String>>(
                                (item) => DropdownMenuItem(
                                      value: item,
                                      child: Text(item),
                                    ))
                            .toList(),
                        onChanged: (value) {
                          setState(() {
                            role = value;
                          });
                        }),
                  ),
                ],
              ),
            ),
            CustomButton(
              color: Theme.of(context).accentColor,
              text: 'Register',
              callback: _registerUser,
            ),
          ],
        ),
      ),
    );
  }
}
