import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:kidslearn/models/user_model.dart';
import 'package:kidslearn/utils.dart';
import 'package:kidslearn/widgets/custom_button.dart';
import 'package:kidslearn/widgets/nav_drawer.dart';
import 'package:uuid/uuid.dart';

class NewClassScreen extends StatefulWidget {
  @override
  _NewClassScreenState createState() => _NewClassScreenState();
}

class _NewClassScreenState extends State<NewClassScreen> {
  String name, id1, id2, id3, password;
  final Firestore _firestore = Firestore.instance;

  TextEditingController nameController = TextEditingController();
  TextEditingController id1Controller = TextEditingController();
  TextEditingController id2Controller = TextEditingController();
  TextEditingController id3Controller = TextEditingController();
  TextEditingController passController = TextEditingController();

  final User prof = getCurrentUser();

  _createClass() async {
    if (nameController.text.length > 0 &&
        id1Controller.text.length > 0 &&
        id2Controller.text.length > 0 &&
        id3Controller.text.length > 0 &&
        passController.text.length > 0) {
      var resp = await _firestore.collection('classes').add({
        'profs': [prof.uid],
        'studs': [],
        'name': name,
        'id1': id1,
        'id2': id2,
        'id3': id3,
        'pass': password
      });

      await _firestore.collection('users').document(prof.uid).get().then((query) {
        User upUser = User.fromJson(query.data);
        upUser.classes.add(resp.documentID);
        _firestore.collection('users').document(prof.uid).setData(upUser.toJson());
      });

      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.menu),
          iconSize: 30.0,
          onPressed: () {},
        ),
        title: Text(
          'Create New Class',
          style: TextStyle(
            fontSize: 28.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Container(
        color: Theme.of(context).backgroundColor,
        padding: EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                onChanged: (value) => name = value,
                decoration: InputDecoration(
                    hintText: 'Enter Class Name', border: OutlineInputBorder()),
                controller: nameController,
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                autocorrect: false,
                onChanged: (value) => id1 = value,
                decoration: InputDecoration(
                    hintText: 'Enter ID filed 1', border: OutlineInputBorder()),
                controller: id1Controller,
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                autocorrect: false,
                onChanged: (value) => id2 = value,
                decoration: InputDecoration(
                    hintText: 'Enter ID filed 2', border: OutlineInputBorder()),
                controller: id2Controller,
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                autocorrect: false,
                onChanged: (value) => id3 = value,
                decoration: InputDecoration(
                    hintText: 'Enter ID filed 3', border: OutlineInputBorder()),
                controller: id3Controller,
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              color: Theme.of(context).primaryColor,
              child: TextField(
                autocorrect: false,
                onChanged: (value) => password = value,
                decoration: InputDecoration(
                    hintText: 'Enter Class Passprhase',
                    border: OutlineInputBorder()),
                controller: passController,
              ),
            ),
            SizedBox(
              height: 40.0,
            ),
            CustomButton(
              color: Theme.of(context).accentColor,
              text: 'Create Class',
              callback: _createClass,
            ),
          ],
        ),
      ),
    );
  }
}
