import 'package:localstorage/localstorage.dart';
import 'package:kidslearn/models/user_model.dart';

//class Utils {

  getSavedUsers() {
    LocalStorage storage = new LocalStorage('state');
    List<User> users = (storage.getItem('users') as List<Map<String, dynamic>>).map((item) => User.fromJson(item)).toList();
    return users;
  }

  setCurrentUser(User user) async {
    LocalStorage storage = new LocalStorage('state');
    storage.setItem('current_user', user.toJson());
  }

  getCurrentUser() {
    LocalStorage storage = new LocalStorage('state');
    return User.fromJson(storage.getItem('current_user') as Map<String, dynamic>);
  }
//}